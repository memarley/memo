package Sucursal;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

public class Cnn {
    
    private static String NOMBRE_BD = "lavanderia";
    private static String USUARIO_BD = "root";
    //    private static String USUARIO_BD = "jsoto";
    private static String CLAVE_BD = "bascula1101";
    //    private static String CLAVE_BD = "js123";
    private static String DRIVER_BD = "com.mysql.jdbc.Driver";
    private static Connection conn;
    
    public static Connection getConexion() {
        try {
            Class.forName(DRIVER_BD);
            if(conn == null)conn = DriverManager.getConnection("jdbc:mysql://localhost/" + NOMBRE_BD, USUARIO_BD, CLAVE_BD);
            //            if(conn == null)conn = DriverManager.getConnection("jdbc:mysql://192.168.1.2:3306/" + NOMBRE_BD, USUARIO_BD, CLAVE_BD);
            return conn;            
        }catch(Exception e){            
            System.err.println("Excepción en Cnn.getConexion: " + e);
            JOptionPane.showMessageDialog(null, "Error de conexión a la base de datos","ERROR",0);
            return null;
        }
    }
}
